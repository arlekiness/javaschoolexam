package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

   /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
    	if (statement == null || statement.equals(""))
    		return null;
        String correctInput = takeOutSpace(statement);
        String message = checkOnPrimaryErrors(correctInput);
        if (message.equals("bad"))
            return null;
        else {
            try {
                String g = calculateExpression(correctInput);
                double a = Double.parseDouble(g);
                if (a == (int)a) {
                	Integer b = new Integer((int)a);
                	return b.toString();
                }
                else
                	return g;
            } catch (ArithmeticException ex) {
                return null;
            } catch (NumberFormatException ex) {
                return null;
            }
        }
    }

    
    
    public String[] parseString(String a) {
        String[] rez = new String[3];
        int leftPar = 0;
        int rightPar = 0;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == '(')
                leftPar++;
            if (a.charAt(i) == ')')
                rightPar++;
        }
        
        if ((leftPar == 0) && (rightPar == 0)) {
            rez[0] = rez[2] = "";
            rez[1] = a;
        }
        else {	
            int beginIndex = 0;
            boolean beginIndEvaluate = false;
            int endIndex = 1;
            for (int i = 0; i < a.length(); i++) {
                if ((a.charAt(i) == ')') && (beginIndEvaluate == true)) {
                    endIndex = i;
                    break;
                } else if (a.charAt(i) == '(') {
                    beginIndex = i;
                    beginIndEvaluate = true;
                } else
                    continue;
            }
            
            rez[0] = a.substring(0, beginIndex);
            rez[1] = a.substring(beginIndex + 1, endIndex);
            rez[2] = a.substring(endIndex + 1, a.length());
        }	
        
        return rez;
    }
    //========================================================
    //========================================================
    //========================================================
    //========================================================
    public String checkOnPrimaryErrors(String a) {
        String message = "good";
        int leftPar = 0;
        int rightPar = 0;
        char ch;
        for (int i = 0; i < a.length(); i++) {
            ch = a.charAt(i);
            if (!Character.isDigit(ch) && ch != '+' && ch != '-' && ch != '*' && ch != '/' && ch != '%' && ch != '^' && ch != '(' && ch != ')' && ch != '.') {
                message = "bad";
                break;
            }
            if ((i != 0) && ((ch == '+') || 
                             (ch == '-') || 
                             (ch == '*') || 
                             (ch == '/') || 
                             (ch == '%') || 
                             (ch == '^')) && ((a.charAt(i - 1) == '+') || 
                                              (a.charAt(i - 1) == '-') || 
                                              (a.charAt(i - 1) == '*') || 
                                              (a.charAt(i - 1) == '/') || 
                                              (a.charAt(i - 1) == '%') || 
                                              (a.charAt(i - 1) == '^'))) {
                message = "bad";
                break;
            }
            if ((ch == ')') && (i == 0)) {
                message = "bad";
                break;
            }
            if ((ch == '(') && (i == a.length() - 1)) {
                message = "bad";
                break;
            }
            if ((i != 0) && (ch == ')') && (a.charAt(i - 1) == '(')) {
                message = "bad";
                break;
            }
            if ((i != 0) &&(ch == '(') && (a.charAt(i - 1) == ')')) {
                message = "bad";
                break;
            }
            if ((ch == '(') && ((a.charAt(i + 1) == '+') || 
                                (a.charAt(i + 1) == '*') || 
                                (a.charAt(i + 1) == '/') || 
                                (a.charAt(i + 1) == '%') || 
                                (a.charAt(i + 1) == '^'))) {
                message = "bad";
                break;
            }
            if ((ch == ')') && ((a.charAt(i - 1) == '+') || 
                                (a.charAt(i - 1) == '-') || 
                                (a.charAt(i - 1) == '*') || 
                                (a.charAt(i - 1) == '/') || 
                                (a.charAt(i - 1) == '%') ||
                                (a.charAt(i - 1) == '^'))) {
                message = "bad";
                break;
            }
            if (ch == '(')
                leftPar++;
            if (ch == ')')
                rightPar++;
        }
        
        if (leftPar != rightPar && (message == "good")) {
            message = "bad";
        }
        
        return message;
    }
    //========================================================
    //========================================================
    //========================================================
    //========================================================
    public static String takeOutSpace(String a) {
        StringBuilder b = new StringBuilder("");
        for (int i = 0; i < a.length(); i++) {
            char ch = a.charAt(i);
            if (ch != ' ' && ch != '\t')
                b.append(ch);
        }
        return b.toString();
    }
    //========================================================
    //========================================================
    //========================================================
    //========================================================
    public String calculateExpression (String a) throws ArithmeticException, NumberFormatException {
        String[] parsed = parseString(a);
        if (parsed[0].equals("") && parsed[2].equals("") && isNumber(a))
            return a;
        else {
            boolean isSimpleString = true;
            
            for (int i = 0; i < parsed[1].length(); i++) {
                if ((parsed[1].charAt(i) == '(') || (parsed[1].charAt(i) == ')')) {
                    isSimpleString = false;
                    break;
                }
            }
            
            if (isSimpleString) {
                parsed[1] = calculateSimpleString(parsed[1]);
            } else {
                parsed[1] = calculateExpression(parsed[1]);
            }

            a = parsed[0] + parsed[1] + parsed[2];
            return calculateExpression(a);
        }
    }
    
    //========================================================
    //========================================================
    //========================================================
    //========================================================
    
    public boolean isNumber(String a) {
        boolean sign;
        if (a.charAt(0) == '-')
            sign = true;
        else
            sign = false;
        
        if (sign) {
            for (int i = 1; i < a.length(); i++) {
                char ch = a.charAt(i);
                if (!Character.isDigit(ch) && ch != '.')
                    return false;
            }
        } else {
            for (int i = 0; i < a.length(); i++) {
                char ch = a.charAt(i);
                if (!Character.isDigit(ch) && ch != '.')
                    return false;
            }
        }
        
        return true;
    }
    //========================================================
    //========================================================
    //========================================================
    //========================================================
    public int priorOp(char ch) {
        int a;
        switch (ch) {
            case '+': case '-': 
                a = 1;
                break;
            case '*': case '/': case '%': 
                a = 2;
                break;
            case '^': 
                a = 3;
                break;
            default:
                a = -1;
                break;
        }
        return a;
    }
    //========================================================
    //========================================================
    //========================================================
    //========================================================
    public String calculateSimpleString (String a) throws ArithmeticException, NumberFormatException {
        ArrayList<Double> numbers = new ArrayList<>();
        ArrayList<Character> signs = new ArrayList<>();
        int i = 0;
        while (i < a.length()) {
            char ch = a.charAt(i);
            if (((i == 0) && (ch == '-')) || ((ch == '-') && !Character.isDigit(a.charAt(i - 1)))) {
                StringBuilder num = new StringBuilder("");
                num.append(ch);
                i++;
                while ((i < a.length()) && (Character.isDigit(a.charAt(i)) || (a.charAt(i) == '.'))) {
                    num.append(a.charAt(i));
                    i++;
                }
                Double r = Double.parseDouble(num.toString());
                numbers.add(r);
            } else if (Character.isDigit(ch)) {
                StringBuilder num = new StringBuilder("");
                num.append(ch);
                i++;
                while ((i < a.length()) && (Character.isDigit(a.charAt(i)) || (a.charAt(i) == '.'))) {
                    num.append(a.charAt(i));
                    i++;
                }
                Double r = Double.parseDouble(num.toString());
                numbers.add(r);
            } else {
                signs.add (new Character(ch));
                i++;
            }
                
        }
        
        
        while (numbers.size() > 1 && signs.size() > 0) {
            int priority = 0;
            int index = -1;
            for (int l = 0; l < signs.size(); l++) {
                int check = priorOp(signs.get(l));
                if (check > priority) {
                    priority = check;
                    index = l;
                }
            }
            double result;
            double one = numbers.get(index);
            double two = numbers.get(index + 1);
            char ch = signs.get(index);
    
            switch(ch) {
                case '+':
                    result = one + two;
                    break;
                case '-':
                    result = one - two;
                    break;
                case '*':
                    result = one * two;
                    break;
                case '/':
                    if (two == 0) throw new ArithmeticException ();
                    else {
                        result = one / two;
                        break;
                    }
                case '%':
                    if (two == 0) throw new ArithmeticException ();
                    else {
                        int three = (int)one % (int) two;
                        result = three;
                        break;
                    }
                case '^':
                    result = Math.pow(one, two);
                    break;
                default:
                    result = 1;
            }
            signs.remove(index);
            numbers.remove(index);
            numbers.remove(index);
            numbers.add(index, result);
        }
        
        
        
        return numbers.get(0).toString();
        
    }

}
