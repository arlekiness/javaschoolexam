package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null)
			   throw new IllegalArgumentException();
		   
		   boolean isAlright = true;   
	
			if (x.isEmpty())
				return isAlright;
				  
		
			if (y.isEmpty() && !x.isEmpty()) {
				isAlright = false;
				return isAlright;
			}
			  
		    int xSize = x.size();
		    int ySize = y.size();
		    if (xSize > ySize) {
		    	isAlright = false;
		    	return isAlright;	
		    } 
		      
		    int offset = 0;
		    int numOfCoins = 0;
		      
		    int i = 0;
		    while (i < ySize && numOfCoins < xSize) {
		    	if (x.get(offset).equals(y.get(i))) {
		    		offset++;
		    		i++;
		    		numOfCoins++;
		    	} else {
		    		i++;
		    	}	    		  
		    }
		      
		    if (numOfCoins < xSize)
		    	isAlright = false;
		      
		    return isAlright;
    }
}
