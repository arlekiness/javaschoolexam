package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;;

public class PyramidBuilder {

     /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
    	if(inputNumbers == null)
    		throw new CannotBuildPyramidException("");
    	
        for (Integer elem : inputNumbers) {
        	if (elem == null)
        		throw new CannotBuildPyramidException("Пустой элемент в пирамиде");
        }
        
        int pHeight = pyramidHeight(inputNumbers);
        if (pHeight > Integer.MAX_VALUE/2 || pHeight < 0)
        	throw new CannotBuildPyramidException("Превышен лимит");
        int pWidth = pHeight * 2 - 1;
        
        int[][] pyramid = new int[pHeight][pWidth];
        
        Collections.sort(inputNumbers);
        
        int offset = 0;
        for (int i = 0; i < pHeight; i++) {
        	int j = 0;
        	
        	for (; j < pHeight - i - 1; j++) {
        		pyramid[i][j] = 0;
        	}
        	
        	int k = 0;
        	for (; k < (i + 1) * 2 - 1; k++) {
        		if ((k % 2) == 0) {
        			pyramid[i][k + j] = inputNumbers.get(offset);
        			offset++;
        		} else {
        			pyramid[i][k + j] = 0;
        		}
        	}
        	
        	j += k;
        	
        	for(;j < pWidth; j++) {
        		pyramid[i][j] = 0;
        	}
        }
        
        return pyramid;
    }

    
    
    public int pyramidHeight(List<Integer> inputNumbers) {
    	int floors = 0;
    	int sum = 0;
    	int term = 1;
    	while (sum < inputNumbers.size()) {
    		floors++;
    		sum += term;
    		term++;
    	}
    	
    	if (sum == inputNumbers.size())
    		return floors;
    	else
    		throw new CannotBuildPyramidException("Невозможно построить симметричную пирамиду");
    }


}
